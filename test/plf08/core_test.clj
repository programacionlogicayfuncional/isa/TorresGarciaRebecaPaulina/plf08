(ns plf08.core-test
    (:require [clojure.test :refer :all]
              [plf08.core :refer :all]))

(deftest prueba1-test
  (testing ""
    (is (= "ejemplo1" (plf08.archivo/descifrar "resources/ejemplo1.txt")))))
(deftest prueba2-test
  (testing ""
    (is (= "ejemplo2" (plf08.archivo/descifrar "resources/ejemplo2.txt")))))
