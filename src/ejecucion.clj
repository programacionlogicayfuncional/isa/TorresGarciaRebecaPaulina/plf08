(ns ejecucion
  (:gen-class)
  (:require [plf08.polibio :as squer]
            [plf08.archivo :as txt]
            [clojure.string :as string]))
 

(defn cifrar
  [c p]
  (archivo/cifrar c (string/join (map squer/cuadrado (archivo/leer p)))))
(cifrar "resources/ejemplo3.txt" "resources/ejemplo3.txt")
(cifrar "resources/ejemplo1.txt" "resources/ejemplo1.txt")

(defn descifrar
  [c p]
  (archivo/escribir c (string/join (map squer/cuadrado (vec (map (partial apply str)
                                      (partition-all 2 (archivo/leer p))))))))
(descifrar "resources/ejemplo1.txt" "resources/ejemplo1.txt")
(descifrar "resources/ejemplo4.txt" "resources/ejemplo4.txt")
 
(defn -main
  [a b c]
  (if (= a "Cifrado Polibio")
    (cifrar b c)
    (descifrar b c)))
(-main "descifrar" "resources/ejemplo2.txt" "resources/ejemplo2.txt")
(-main "cifrar" "resources/ejemplo1.txt" "resources/ejemplo1.txt")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;,,cifrado2;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
