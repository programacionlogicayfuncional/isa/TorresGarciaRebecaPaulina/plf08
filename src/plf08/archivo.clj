(ns plf08.archivo)
(defn leer
  [c]
  (let [p (slurp c)]
    p))

(defn escribir
  [c p]
  (spit c p))


(defn cifrar
  [c p]
  (escribir c (apply str (map plf08.polibio/cuadrado p))))

(defn descifrar
  [p]
  (apply str (map plf08.polibio/cuadrado (into [] (map (partial apply str) (partition-all 2 (leer p)))))))
